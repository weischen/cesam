#!/usr/bin/env python2.7

"""
Usage: cesam.py [ --help | --version ]


"""
from __future__ import print_function
from __future__ import division
from docopt import docopt
arguments = docopt(__doc__, version='version 0.0.1')
if arguments['--help']:
    print (arguments)

import os,sys,re,gzip,csv, subprocess, time, glob, datetime
if not os.path.exists('config.ini'):
    print ("a config.ini file is required. You can recreate the default ini file using\n    python modules/generate_ini.py\n\nExiting")
    sys.exit(0)
from collections import defaultdict
import ConfigParser
from itertools import islice
import numpy as np
import pandas as pd
from scipy import stats
import json
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True}) 
import statsmodels.stats.multitest as smm
import statsmodels.api as sm
import scipy.stats as stats
from pybedtools import BedTool
import pybedtools
import seaborn as sns
from collections import Counter
today = datetime.date.today().strftime("%y%m%d")
cesam_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(cesam_dir +  "/modules/")
print (cesam_dir)
# own modules
from zip_tabix import zip_tabix
from cesam_plots import pca_plot, plot_manhattan, plot_pval, make_boxplot
from process_phenotype import phenotype_preprocess
from process_genotype import genotype_matrix_pruning
from qqplot import qqplot, estimate_lambda
'''
Linear model: Phe ~ Var + Cov
'''

#####################################################################################################
##################################### configuration files ###########################################
#####################################################################################################
config = ConfigParser.SafeConfigParser()
config.read('config.ini')

project = config.get("user_setting", 'project')
window = config.get("default_setting", "window")
permute = config.get("default_setting", "permute")
percent_chrom_eigens = config.get("default_setting", "n_pca")
data_dir = config.get("user_setting", 'data_dir')
workdir = config.get("user_setting", 'work_dir')
project_dir = config.get("user_setting", 'project_dir')
genotype_dir = config.get("user_setting", 'genotype_dir')
expression_file = config.get("user_setting", 'expression_matrix')
phenotype_log = config.getboolean("user_setting", "expression_matrix_transform")
sv_call_file = config.get("user_setting", 'sv_call_file')
geno_raw_vcf = config.get("user_setting", 'breakpoint_matrix')
sample_tumor_relationship = config.get("user_setting", 'sample_tumor_relationship')
gencode_file  = config.get("user_setting", 'gencode_file')

print ("\n\nproject:",project, "\nwindow:", window, "\npermute:", permute, "\ndata dir:", data_dir, "\nworkdir:", workdir, "\nproject dir:", project_dir, "\ngenotype dir:", genotype_dir, 
"\nexpression_matrix:", expression_file, "\nis phenotype_file log transformed", phenotype_log, "\nsv_call_file:", sv_call_file, "\ngenotype vcf (raw):",geno_raw_vcf)
plot_dir = workdir + "/plots"
if not os.path.exists(plot_dir):
    os.makedirs(plot_dir)

bin_dir = cesam_dir + "/bin/"
#sys.exit(0)

print ("#"*40 ,"project is", project, "#" *40, "\n\n")

if not os.path.exists(project_dir):
    os.makedirs(project_dir)

if not os.path.exists(genotype_dir):
    os.makedirs(genotype_dir)

os.chdir(project_dir)

merged_tumor_types = ['TCGA_KIPAN', 'TCGA_STES', 'TCGA_GBMLGG', 'TCGA_COADREAD']
sample_tumor_relationship_dict = dict()
with open(sample_tumor_relationship) as rin:
    for f in rin:
        row = f.rsplit()
        if len(row[1].split(";"))>1:
            tumortype = list(set(row[1].split(";")).intersection( set(merged_tumor_types)))[0]
            if not tumortype:
                tumortype = row[1].split(";")[0]
        else:
            tumortype = row[1]
        sample_tumor_relationship_dict[row[0]] = tumortype


'''
G E N O T Y P E
write out the genotype (SV intersection) file
'''

print ("genotype file", geno_raw_vcf)

'''
get list of pids to genotype col
'''

vcf_header = list()

geno_vcf = geno_raw_vcf.split(".vcf")[0] + "_corr.vcf"
with gzip.open(geno_raw_vcf, 'rb') as fileIn, open(geno_vcf, 'w') as fileOut:
    for f in fileIn:
        row = f.rsplit()
        if row[0].startswith('##'):
            row = ' '.join(map(str, row)) + '\n'
            if row.startswith('##INFO'):
                row = row.replace("TYPE=INFO", "ID=INFO")
            fileOut.writelines(row)
        elif row[0].startswith('#'):
            row = '\t'.join(map(str, row)) + '\n'
            fileOut.writelines(row)
        else:
            rowmod = [i.replace('0/1', '1/1') for i in row]
            rowmod[7] = 'INFO=' + rowmod[7]
            rowmod = '\t'.join(map(str, rowmod)) + '\n'
            wt = rowmod[10:].count('0/0')
            het = rowmod[10:].count('1/1')
            if (het+wt >= 100 and het>3 and het/(het+wt) > 0.01) or \
            (het+wt < 100 and het>2):
                fileOut.writelines(rowmod)


geno_vcf = zip_tabix(geno_vcf, p="vcf")

with gzip.open(geno_vcf, 'rb') as r:
    for l in r:
        if l.startswith('#CHROM'):
            row = l.rstrip().split('\t')
            geno_vcf_samples = list(set(row[9:]))
            break



tad_list = list()
with gzip.open(geno_vcf) as r:
    for f in r:
        row = f.split()
        if not row[0].startswith('#'):
            chrom = row[2].split(':')[0]
            start = row[2].split(':')[1].split('-')[0]
            end = row[2].split(':')[1].split('-')[1].split(';')[0]
            feature = row[2].split(';')[1]
            genotypes = row[8:]
            wt = genotypes.count('0/0')
            het = genotypes.count('1/1')
            fraction = het/(wt+het)
            tad_list.append([row[2], het, wt, fraction])

plotNameTadBreak = project + '_break_per_TAD_hist.pdf'
df_tad = pd.DataFrame(tad_list)
df_tad.sort_values(by=3, ascending=False, inplace=True)
df_tad.columns = ['sid', 'n_het', 'n_wt', 'break_per_tad']
try:
    ax = df_tad["break_per_tad"].plot(kind="hist", bins=30)
    ax.set_title(project + " Breaks per TAD")
    plt.savefig(plotNameTadBreak)
    plt.close()
except Exception, e:
    pass


df = df_tad
df['break_per_tad_percentile'] = df_tad['break_per_tad'].apply(lambda x: round(stats.percentileofscore(df_tad['break_per_tad'], x)))
top_tad_break = df.ix[df['break_per_tad_percentile'] >=95, 'sid'].tolist()

geno_vcf_prune = geno_vcf.replace('.vcf.gz', '_pruned.vcf')

geno_vcf_prune, geno_vcf_pca_remove, pca_sv_file =  genotype_matrix_pruning(geno_vcf, sv_call_file, sample_tumor_relationship_dict, top_tad_break, project, project_dir)

####################################################################################################
## PHENOTYPE
####################################################################################################
if not phenotype_log:
    phenotype_fqtl = re.sub(r'.bed.*', r'_corr.bed', expression_file)
    phenotype_preprocess(expression_file, phenotype_fqtl, var_percentile=20, logtransform=True, sample_remove=None)
    phenotype_fqtl = zip_tabix(phenotype_fqtl, p="bed")
else:
    phenotype_fqtl = expression_file



######################################################################################################
# FASTQTL
####################################################################################################

print ("#" * 40, "\n\nRunning linear regression")

# ## PRUNED SET ##

## NORMALIZE PHENOTYPE, ADD WINDOW AND ADD COVARIATES ##

def remove_missing_pheno(pheno_bed, geno_vcf, suffix="_remove_missing_pheno"):
    # Remove samples from phenotype sample
    # project_dir + "/" + pheno_prune_bed
    genoSamples = []
    with gzip.open(geno_vcf) as fileIn:
        for f in fileIn:
            row = f.rsplit()
            if row[0].startswith('#CHROM'):
                genoSamples = row
                break
    # pheno_bed = phenotype_fqtl
    df_phenotype = pd.read_csv(pheno_bed, sep="\t")
    df_phenotype_prune = df_phenotype.filter(items=df_phenotype.columns.tolist()[:4] +  genoSamples)
    pheno_prune_bed = pheno_bed.replace('.bed.gz', suffix + ".bed")
    df_phenotype_prune.to_csv(pheno_prune_bed, sep="\t", index=False)
    pheno_prune_bed = zip_tabix(pheno_prune_bed, p="bed")
    return pheno_prune_bed



fastQTL = bin_dir + "fastQTL"

genotype_fq=geno_vcf_prune

## RUN ITERATION
def regression_run(genotype_fq, phenotype_fqtl, pca_sv_file, gencode_file,  permute=1000, percent_chrom_eigens=20, window="2e6", qtlannotate=False):
    result_dir = project_dir + "/fastqtl_result_pca" + str(percent_chrom_eigens)
    missed_chrom = set()
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    genoname = os.path.basename(genotype_fq).replace(".vcf.gz", "")
    qtl_report_file = result_dir + "/" + str(today) + "_" + genoname + "_pca" + str(percent_chrom_eigens) + "_report_file.md"
    report_open = open(qtl_report_file, 'w')
    report_open.writelines("# FASTQTL Analysis on " + project + "\n" + today + "\n\n\n")
    os.chdir(result_dir)
    chrom_dir = result_dir + '/fastqtl_chrom/'
    print ("input genotype file", genotype_fq, "\ninput phenotype file", phenotype_fqtl)
    if os.stat(genotype_fq).st_size == 0:
        print (genotype_fq, "empty")
        return 'NA'
    phenotype_fq = remove_missing_pheno(phenotype_fqtl, genotype_fq, suffix="_removeMissingPid")
    CmdTabix = "tabix -f -p bed " + phenotype_fq
    subprocess.Popen(CmdTabix.split())
    report_open.writelines("-"*40  + " \n\n## " + os.path.basename(genotype_fq) + "\n\nfiles:\ngenotype:\t" + genotype_fq + "\nphenotype:\t" + phenotype_fq)
    ## Extract eigen vectors
    chrom_eigens = '0'
    eigens = list()
    if os.path.exists(pca_sv_file):
        with open(pca_sv_file) as eigenIn:
            for f in eigenIn:
                row = f.rsplit()
                eigens.append(row)
        max_eigens = len(eigens)
        ## Choose here what percentage of the total number of PCAs to include - 100 == all
        chrom_eigens = int(max_eigens * (percent_chrom_eigens/100.0))
        report_open.writelines("max eigens:\t" + str(max_eigens) + "\nused number of eigens:\t" + str(chrom_eigens) + "\n\n")
        plink_eigenvec_pca_sub = pca_sv_file.replace('.txt', '_pca' + str(chrom_eigens) + '.txt')
        print (max_eigens, "PCAs for chromosomes\n" + str(chrom_eigens) + " selected\nPCA subset file\n" + plink_eigenvec_pca_sub)
        n = 0
        with open(plink_eigenvec_pca_sub, 'w') as eigenOut:
            for row in eigens:
                n += 1
                if n <= chrom_eigens:
                    eigenOut.writelines('\t'.join(map(str,row)) + "\n")
    ##
    if not os.path.exists(chrom_dir):
        os.makedirs(chrom_dir)
    for c in range(1, 23) +  ['X', 'Y', 'M']:
        chrom = 'chr' + str(c)
        report_open.writelines(chrom + "\n")
        print ("\nanalyzing",chrom)
        if not os.path.exists(plink_eigenvec_pca_sub):
            cmdFQTL = fastQTL + ' --vcf ' + genotype_fq + ' --bed ' + phenotype_fq + '  --permute ' + str(permute)   + " --normal"
        else:
            cmdFQTL = fastQTL + ' --vcf ' + genotype_fq + ' --bed ' + phenotype_fq + '  --permute ' + str(permute)   + ' --cov ' + plink_eigenvec_pca_sub + " --normal"
        # iterate over all chrom
        sv_fastqtl_win =  chrom_dir + os.path.basename(genotype_fq).replace('.vcf.gz', "_PCA_" + chrom + "_ciswin" + str(window) + "_pca" + str(chrom_eigens) + '.txt')
        sv_fastqtl_log = sv_fastqtl_win.replace('.txt',  "_" + str(today) + '.log')
        cmdFQTLWin = cmdFQTL + ' --region ' + chrom + '  --window ' + str(window)  + ' --out ' + sv_fastqtl_win + ' --log ' + sv_fastqtl_log
        if os.path.exists(sv_fastqtl_win):
            os.remove(sv_fastqtl_win)
        subprocess.Popen(cmdFQTLWin.split())
        try:
            os.wait()
        except Exception, e:
            pass
        if os.path.exists(sv_fastqtl_win) and os.stat(sv_fastqtl_win).st_size == 0:
            time.sleep(3)
            errmess = (chrom + " failed. Retry without normalization")
            print (errmess)
            report_open.writelines(errmess + "\n")
            cmdFQTLRaw = fastQTL + ' --vcf ' + genotype_fq + ' --bed ' + phenotype_fq + '  --permute ' + str(permute)
            cmdFQTLWin = cmdFQTLRaw + ' --region ' + chrom + '  --window ' + str(window)  + ' --out ' +  sv_fastqtl_win  + ' --log ' + sv_fastqtl_log
            subprocess.Popen(cmdFQTLWin.split())
            try:
                os.wait()
            except Exception, e:
                pass
        if os.path.exists(sv_fastqtl_win) and os.stat(sv_fastqtl_win).st_size == 0:
            time.sleep(3)
            errmessnonorm = (chrom + " failed without normalization")
            print (errmessnonorm)
            report_open.writelines(errmessnonorm + "\n")
            missed_chrom.add(chrom)
            with open(sv_fastqtl_log, 'a') as logOut:
                logOut.writelines('\n\nno normalization. \nParams: \n\n' + cmdFQTLWin)
                report_open.writelines("fastQTL command:\n" + cmdFQTLWin + "\n\n")
        while not os.path.exists(sv_fastqtl_win):
            time.sleep(1)

    time.sleep(30)

    combiFile = result_dir + "/" + project + "_ciswin"  + str(window) + "_merge.txt"
    print ('\n\ncombine chromosomes', combiFile)
    chromiter = re.sub(r"_PCA_.*_ciswin", "_PCA_*_ciswin", sv_fastqtl_win)
    filenames = glob.glob(chromiter)
    with open(combiFile, 'w') as outfile:
        for fname in filenames:
            with open(fname, 'rb') as infile:
                for line in infile:
                    if not 'NA' in line:
                        outfile.write(line)
    print ("generate", combiFile)
    df = pd.read_csv(combiFile, sep=" ", error_bad_lines=False, header=None)
    df.dropna(axis=0, inplace=True)
    colnames = ["pid", "nvar", "shape1", "shape2", "dummy", "sid", "dist", "npval", "slope","ppval", "bpval"]
    df.columns = colnames

    df_take_winner = pd.DataFrame()
    for i, j in df.groupby('sid'):
        j.sort_values("bpval")
        top_gene = j.sort_values("bpval").head(1)
        df_take_winner = df_take_winner.append(top_gene)
    alpha = 0.05
    methods = ["fdr_bh", "fdr_by", 'bonferroni']
    for method in methods:
        rej, pval_corr = smm.multipletests(df['bpval'], alpha=alpha, method=method)[:2]
        rej_winner, pval_corr_winner = smm.multipletests(df_take_winner['bpval'], alpha=alpha, method=method)[:2]
        df[method] = pval_corr
        df_take_winner[method] = pval_corr_winner
    gencode = pd.read_csv(gencode_file, sep="\t")
    gencode.columns = ['#chrom', 'start', 'end', 'gene_name', 'strand']
    df_gc = pd.merge(gencode, df, left_on='gene_name', right_on="pid")
    df_gc_take_winner = pd.merge(gencode, df_take_winner, left_on='gene_name', right_on="pid")
    combiGencodeOut = combiFile.replace('.txt', '_gencode_fdr.bed')
    combiGencodeOutWinner = combiFile.replace('.txt', '_take_winner_gencode_fdr.bed')
    df_gc.sort_values("bpval", inplace=True)
    df_gc.to_csv(combiGencodeOut, sep="\t", index=False)
    df_gc_take_winner.to_csv(combiGencodeOutWinner, sep="\t", index=False)
    print ("save ",combiGencodeOut)
    sign_genes = set(df_gc.ix[df_gc.fdr_bh<0.1, 'pid'])
    n_sign_genes = str(len(sign_genes))
    report_open.writelines("combined result file:\n" + combiGencodeOut + "\n\nNumber of gene candidates:\t" \
        + str(len(set(df.pid))) + "\nNumber of gene candidates below FDR BH 10%:\t" + n_sign_genes \
        + "\ngenes:\t" + ';'.join(map(str, df_gc.ix[df_gc.fdr_bh<0.10, 'pid'].ravel())) + "\n\n")
    # from qqplot import qqplot
    pvals = df_gc.bpval.tolist()
    ginflate_lambda = estimate_lambda(pvals)
    if ginflate_lambda:
        print ("\n\n>>", ginflate_lambda)
    report_open.writelines("**Genomic Inflation Factor (lambda): " + str(ginflate_lambda) + "**\n\n")
    # with open(combiFile) as infile:
    qqplot_dir = plot_dir + '/qqplot/'
    if not os.path.exists(qqplot_dir):
        os.makedirs(qqplot_dir)
    os.chdir(qqplot_dir)
    goi = project
    plotName = os.path.basename(combiGencodeOut)
    qqplot(pvals, goi, plotName)
    plot_manhattan(combiGencodeOut)
    ## Plot pval ##
    plot_pval(combiFile, df)

    os.chdir(result_dir)
    report_open.close()
    if qtlannotate:
        qtl_stat(df_gc, combiFile, sv_call_file, df_tad, phenotype_fq)
        print ("DONE")
    return ({"gi_lambda": ginflate_lambda, "n_sign_genes": n_sign_genes, "missed_chrom": missed_chrom, "sign_genes": ';'.join(map(str, list(sign_genes)))})


regression_run(genotype_fq, phenotype_fqtl, pca_sv_file, gencode_file,  permute=1000, percent_chrom_eigens=20, window="2e6", qtlannotate=False)


#listOfGenoVcf = glob.glob(genotype_dir + "/*" + project + "*.vcf.gz")
#os.chdir(project_dir)
#CmdTabix = "tabix -f -p bed " + phenotype_fqtl
#subprocess.Popen(CmdTabix.split())
#time.sleep(3)

# estimate_dict = defaultdict(dict)
