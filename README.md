# CESAM - cis expression structural alteration mapping #

code associated with the Nature Genetics paper [doi:10.1038/ng.3722](http://www.nature.com/ng/journal/v49/n1/full/ng.3722.html)

* Version 0.0.1

The current version will apply pre-processing and regression analysis of a breakpoint occurrence matrix (genotype) and an expression matrix (phenotype). CESAM's modules to apply post-regression filters will be added soon.  
Generation of the genotype and phenotype matrices have to be provided. Example files are present in the `data` folder.

### Requirements ###

CESAM runs with python 2.7. 

In addition, a number of binaries and python modules are required. 

* plink v1.90 [link](https://www.cog-genomics.org/plink2)  
* tabix v1.2.1 [link](http://www.htslib.org/doc/tabix.html)
* bedtools v2.26 [link](bedtools.readthedocs.io)
* vcflib [link](https://github.com/vcflib/vcflib)  
* fastQTL v2.1 [link](http://fastqtl.sourceforge.net/)  

plink, vcflib and fastqtl are present as precompiled binaries (GNU/Linux 2.6) in `bin` dir. Substitute these files with compiled binaries, if you are working on another dist.  

Required python module are in the `requirements.txt` file and can be installed using:

```
pip install -r requirements.txt
```

alternatively using the [conda package manager](https://www.continuum.io/downloads) e.g.  
`while read req; do conda install -y $req; done < requirements.txt; conda install -c bioconda pybedtools`



### configure ###

#### breakpoint occurrence matrix  
Prepare a genotype matrix VCF file based on bins of interest (e.g. TAD bins) by annotating for every sample the presence or absence of breakpoints within a a bin. Positional information in the VCF file should be the center of each bin. CESAM will prune the file for genotype bins with fewer than x occurrences, and perform TAD bin merging for neighbour bins with similar genotypes.  
An example breakpoint occurrence vcf file is present in the data directory "sv_TAD_TCGA_COADREAD_genome_bin_genotype_raw.vcf.gz". When using the TCGA data to generate the breakpoint occurrence matrix, we additionally removed SCNAs that overlapped with non-tumour SNP6 copy number data samples (75% reciprocal overlap).  
Prepare a **sv call set file**, which should contain all the breakpoints in BED format. An example sv call file is present in the data directory "data/sv_TAD_TCGA_COADREAD_sv_calls.bed.gz"  

#### dosage-adjusted expression matrix file  
Prepare a log2 normalised dosage-adjusted expression matrix in BED format (e.g. gene coordinates and associated FPKM values for each sample), by dividing each gene’s expression (before log2 transformation) by the tumor/normal gene copy-number ratio.
CESAM will prune the expression matrix for low variance genes (remove 20th percentile variance genes) and for genes with more than 50% of samples having 0 values.

#### configure file usage
update the `config.ini` file with your breakpoint occurrence and expression matrices. COADREAD test data is available in the `/data` folder. To test run with the COADREAD dataset,  you need to run config.ini to update your local path `python modules/generate_ini.py`


### execute ###

after configuring the config.ini file, execute by running  
`python cesam.py`

### Citation ###

Please cite:

Weischenfeldt, J., Dubash, T., Drainas, A. P., et al Mardin, B. R., Chen, Y., Stuetz, A. M., Waszak, S. M., Bosco, G., Halvorsen, A. R., Raeder, B., Efthymiopoulos, T., Erkek, S., Siegl, C., Brenner, H., Brustugun, O. T., Dieter, S. M., Northcott, P. A., Petersen, I., Pfister, S. M., Schneider, M., Solberg, S. K., Thunissen, E., Weichert, W., Zichner, T., Thomas, R., Peifer, M., Helland, A., Ball, C. R., Jechlinger, M., Sotillo, R., Glimm, H., and Korbel, J. O.    
[Pan-cancer analysis of somatic copy-number alterations implicates IRS4 and IGF2 in enhancer hijacking](http://www.nature.com/ng/journal/v49/n1/full/ng.3722.html). Nature Genetics 2017 Jan;49(1):65-74. doi: 10.1038/ng.3722.


### License ###

CESAM is distributed under the GPL. Consult the accompanying LICENSE file for more details.