#!/usr/bin/env python
## created: 170102
## by: Joachim Weischenfeldt
## joachim.weischenfeldt@gmail.com
from __future__ import division, print_function
import pandas as pd 
import numpy as np 

def phenotype_preprocess(expression_file, expression_file_pruned, var_percentile=20, logtransform=True, sample_remove=None):
    '''
    prune phenotype file for low variance genes and for genes with more than 50% of samples having 0 values
    input:
        expression_file: expression matrix. First 4 columns has to be chrom, start, end, gene_name
        var_percentile: percentile at which low variance genes are removed (default 20)
        logtransform: if True, will log2 transform values (default True)
        sample_remove: list of sample ids that should be removed from the file (optional)

    output
        expression_file_pruned: pruned expression_file
        phenotype_excluded_genes.txt: genes excluded based on filtering criteria

    '''
    #expression_file_pruned = expression_file.replace(".bed", "_corr.bed")
    df_expression = pd.read_csv(expression_file, sep="\t", low_memory=False)
    header_raw = df_expression.columns
    if sample_remove:
        sample_keep = [x for x in header_raw if x not in sample_remove]
    else:
        sample_keep = header_raw
    df_expression_prune = df_expression.filter(items=sample_keep)
    phenotype_samples = df_expression_prune.columns[4:].tolist()
    df_expression_prune['var'] = df_expression_prune.ix[:, 4:].apply(lambda x: np.var(x), axis=1)
    expr_var_cutoff = np.percentile(df_expression_prune['var'], var_percentile)

    df_expression_var_prune = df_expression_prune.ix[df_expression_prune["var"] > expr_var_cutoff]

    df_out = df_expression_var_prune.ix[df_expression_var_prune[df_expression_var_prune > 0].count(axis=1)/len(phenotype_samples) >0.5, :-1]

    df_out.sort_values(by=header_raw[0:2].tolist(), inplace=True, kind="quicksort", ascending=True)

    df_out.columns = ["#Chr", "start", "end", "TargetID"] + df_out.columns[4:].tolist()
    gene_set = set(df_expression.ix[:, 3])
    gene_set_exclude = list(gene_set - set(df_out.TargetID))

    if logtransform:
        df_out[phenotype_samples] = df_out[phenotype_samples].apply(lambda x: np.log2(x))
    df_out.to_csv(expression_file_pruned, sep="\t", index=False)

    with open('phenotype_excluded_genes.txt', 'w') as fout:
        for i in gene_set_exclude:
            fout.writelines(i + "\n")


    print (("#" *80 + "\n{0} gene excluded out of {1} genes, due to too many absent phenotype data points. \n\nCheck your favorite gene is not in the list\n{2}\n\nfiltered phenotype set: {3}\n\n" + "#" *80).format(len(gene_set_exclude), (len(gene_set_exclude) + len(gene_set)), "phenotype_excluded_genes.txt", expression_file_pruned))



