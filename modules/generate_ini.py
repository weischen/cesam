#!/bin/env/python
from __future__ import print_function
import os
import ConfigParser
cwd = os.path.dirname(os.path.realpath(__file__))
def create_ini(outfile):
    dir = os.getcwd()
    config = ConfigParser.ConfigParser(allow_no_value = True)
    config.add_section("default_setting")
    config.add_section("user_setting")
    config.set("user_setting", '# name the project')
    config.set("user_setting", 'project', 'TCGA_COADREAD')
    config.set("default_setting", '# number of permutations for the regression (use min 100)')
    config.set("default_setting", 'permute', '1000')
    config.set("default_setting", '# Covariates. Percentage of total number of principal components from breakpoint matrix to use. First two co-variates are tumour tissue type and number of SVs in sample.')
    config.set("default_setting", 'n_pca', '20')
    config.set("default_setting", '# cis-window size for identification of phenotypes associated with genotype (centered on midpoint of the genotype bin)')
    config.set("default_setting", 'window', '2e6')
    config.set("user_setting", 'data_dir', dir + '/data')
    config.set("user_setting", 'work_dir', dir + '/analysis')
    config.set("user_setting", 'project_dir', "%(work_dir)s/pheno_normalization")
    config.set("user_setting", 'genotype_dir', "%(work_dir)s/genotypes")
    config.set("user_setting", '# input files')

    config.set("user_setting", '# Expression matrix. Typically derived from RNAseq, consisting of e.g. FPKM/RPKM values')
    config.set("user_setting", 'expression_matrix', '%(data_dir)s/TCGA_COADREAD_rna_genecode.cna_adjust_log.bed.gz')
    config.set("user_setting", '# Should log-transformation be peformed on Expression matrix. Only needed for non-normal distributed expression matrices (e.g. RNAseq data)') 
    config.set("user_setting", 'expression_matrix_transform', 'True') 
    config.set("user_setting", '# SV calls')
    config.set("user_setting", 'sv_call_file', '%(data_dir)s/sv_TAD_TCGA_COADREAD_sv_calls.bed.gz')
    config.set("user_setting", '# vcf file with matrix of breakpoints for each bin. ID column should contain the bin region in standard coordinates and a string to describe the bin type  (e.g. chr1:11000000-12040000;IMR90_TAD)')
    config.set("user_setting", 'breakpoint_matrix', '%(data_dir)s/sv_TAD_TCGA_COADREAD_genome_bin_genotype_raw.vcf.gz')
    config.set("user_setting", '# conversion table to relate sample id to tumour type. Only applicable when analysing samples from different tumour types or different origin')
    config.set("user_setting", 'sample_tumor_relationship', '%(data_dir)s/TCGA_sample_tumor_list.txt')
    config.set("user_setting", '# gencode file with gene coordinates (hg19)')
    config.set("user_setting", 'gencode_file', '%(data_dir)s/gencode.v19.bed.gz')

    with open(outfile, "wb") as ini_file:
        config.write(ini_file)

if __name__ == "__main__":
    rootdir = os.path.dirname(cwd)
    outfile= rootdir + "/config.ini"
    create_ini(outfile)
    print ("created", outfile)
