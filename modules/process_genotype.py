#!/usr/bin/env python
## created: 170102
## by: Joachim Weischenfeldt
## joachim.weischenfeldt@gmail.com
from __future__ import division, print_function
from zip_tabix import zip_tabix
import pandas as pd
import csv
import os
import sys
import time
import subprocess
import gzip
from zip_tabix import zip_tabix    
maindir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

def genotype_matrix_pruning(geno_vcf, sv_call_file, sample_tumor_relationship_dict, top_tad_break, project, project_dir): 
    geno_vcf_prune = geno_vcf.replace('.vcf.gz', '_pruned.vcf')
    df_sv_call = pd.read_csv(sv_call_file, sep="\t", compression="gzip", header=None)

    ####################################################################################################
    # PLINK to remove LD regions. Get best hit for each window
    plink_dir = project_dir + '/plink_analysis/'
    if not os.path.exists(project_dir + '/plink_analysis'):
        os.makedirs(plink_dir)
    os.chdir(plink_dir)
    print (geno_vcf)
    
    plink_tads_keep = list()
    plink_out = plink_dir + "/" + os.path.basename(geno_vcf).replace('.vcf.gz', '') + '.plink'
    cmdLD = maindir + "/bin/plink --vcf " + geno_vcf + "  --maf 0.01 --indep-pairwise 1000kb 200kb 0.2   --out " + plink_out
    subprocess.Popen(cmdLD.split())
    # os.wait()
    time.sleep(3)
    plink_eigenvec = plink_out + '.eigenvec'
    pca_file = plink_eigenvec + ".pca.txt"
    pca_sv_file = plink_eigenvec + ".pca.svs.txt"
    if not os.path.exists(plink_out + '.prune.out'):
        open(pca_file, 'w').close()
        open(pca_sv_file, 'w').close()
        print ("linkage file empty")
        return (np.nan, np.nan, np.nan)
    # output plink.eigenvec
    n_eigens = 5000
    cmdEV = maindir + "/bin/plink --vcf " + geno_vcf + " --extract " + plink_out + ".prune.in --pca " + str(n_eigens) + " --out " +  plink_out
    subprocess.Popen(cmdEV.split())
    while not os.path.exists(plink_eigenvec):
        time.sleep(1)
    ## PCA
    df_eigen = pd.DataFrame.from_csv(plink_eigenvec, sep=" ", header=None)
    n_eigens_extracted = df_eigen.shape[1] -1
    print ("eigen vector file, ", plink_eigenvec, "with eigen vectors of", str(n_eigens_extracted))
    df_eigen_t = df_eigen.T[:n_eigens]
    df_eigen_t.to_csv(pca_file, header=False, sep="\t")
    plink_tads_keep += list(set(line.split('\n')[0] for line in open(plink_out + '.prune.in')))
    ####################################################################################################
    ## Add tumor type ##
    tumor_type_per_sample = [sample_tumor_relationship_dict[i] if i in sample_tumor_relationship_dict else np.nan for i in df_eigen_t.columns]
    df_tumtype = pd.DataFrame(tumor_type_per_sample).T
    df_tumtype.index = ['tumor_type']
    df_tumtype.columns = df_eigen_t.columns
    dfhead = df_eigen_t.ix[:1, :]
    dftail = df_eigen_t.ix[2:, :]
    dftx = pd.concat([dfhead, df_tumtype, dftail])
    dftx.fillna(0, inplace=True)    
    ####################################################################################################
    ## Number of SVs per pid ##
    sv_per_pid = list()
    list_of_ids = list()
    for i, j in df_sv_call.groupby(by=3):
        sv_per_pid.append(len(j.ix[:, 5:].drop_duplicates()))
        list_of_ids.append(i)
    n_sv_per_chrom = pd.DataFrame(sv_per_pid).T
    n_sv_per_chrom.index = ['n_sv_call']
    n_sv_per_chrom.columns = list_of_ids
    dfhead = dftx.ix[:1, :]
    dftail = dftx.ix[1:, :]
    dftx = pd.concat([dfhead, n_sv_per_chrom, dftail])
    dftx.fillna(0, inplace=True)
    dftx.to_csv(pca_sv_file, header=False, sep="\t")
    ####################################################################################################
    try:
        pca_plot(plink_eigenvec, project)
    except Exception, e:
        pass
    # extract and remove samples with high PC1
    # reindex
    dftail.index = range(1, len(dftail.index)+1)
    dftailt = dftail.T
    # take samples with PC1 above 0.1
    hi_pc_samples = dftailt[dftailt[2].abs() > 0.08].index.tolist()
    lo_pc_samples = dftailt[dftailt[2].abs() <= 0.08].index.tolist()

    w = open("hi_pca_samples_" + project + ".ex", 'w')
    w.writelines('\n'.join(map(str,hi_pc_samples)))
    w.close()
    w = open("low_pca_samples_" + project + ".in", 'w')
    w.writelines('\n'.join(map(str,lo_pc_samples)))
    w.close()

    geno_vcf_pca_remove = geno_vcf.replace('.vcf.gz', '_hi_pca_remove.vcf')

    cmdExclude = maindir + "/bin/vcfremovesamples " + geno_vcf + " " + ' '.join(map(str, hi_pc_samples))
    p = subprocess.Popen(cmdExclude.split(), stdout=subprocess.PIPE)
    out, err = p.communicate()
    w = open(geno_vcf_pca_remove, 'wb')
    w.writelines(out)
    w.close()
    geno_vcf_pca_remove = zip_tabix(geno_vcf_pca_remove, p="vcf")
    print ("\n\ngenerated pruned genotype file", geno_vcf_pca_remove)

    ## Remove filtered gene loci from genotype file ##
    #####################################################################
    #                            PRUNING                                 #
    #####################################################################
    if not plink_tads_keep:
        outfile = geno_vcf_prune + '.gz'
        print ("could not complete PLINK pruning\nWill create empty file if not present\n", outfile)
        if not os.path.exists(outfile):
            open(outfile, 'w').close()
        return (outfile, geno_vcf_pca_remove, pca_sv_file)
    plink_tads_keep = list(set(plink_tads_keep))
    with gzip.open(geno_vcf_pca_remove, 'rb') as gin, open(geno_vcf_prune, 'wb') as gout:
        reader = csv.reader(gin, delimiter="\t")
        writer = csv.writer(gout, delimiter="\t", lineterminator="\n")
        for row in reader:
            if row[0].startswith('#'):
                writer.writerow(row)
            if len(row) > 1 and row[2] in plink_tads_keep \
            and row[2] in top_tad_break:
                writer.writerow(row)

    geno_vcf_prune = zip_tabix(geno_vcf_prune)
    print ("\n\ngenerated pruned genotype file", geno_vcf_prune)
    return (geno_vcf_prune, geno_vcf_pca_remove, pca_sv_file)



