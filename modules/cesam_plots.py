#!/bin/env python

from __future__ import print_function
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import subprocess, os
import scipy.stats as stats
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True}) # e



cwd = os.path.dirname(os.path.realpath(__file__))

def plot_manhattan(combiGencodeOut, project="CESAM"):
    manhattan_file = combiGencodeOut.replace('.bed', '_manhattan.pdf')
    cmd = "python " + cwd +"/manhattan-plot.py  --image " \
    + manhattan_file + " --title " +  project + " --cols 0,1,15 " + combiGencodeOut
    p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    out, err = p.communicate()


def pca_plot(plink_eigenvec, project, n_eigens=4):
    '''
    plot PCA of the first 4 eigen vectors of the genotype covariance matrix
    n_eigens = number of eigen vectors to return
    '''
    df = pd.DataFrame.from_csv(plink_eigenvec, sep=" ", header=None)
    df.columns =  ['id'] + ['PC' + str(i+1) for i in range(len(df.columns) -1)]
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(20,10))
    plt.suptitle(project, size=14)
    df.plot(x="PC1", y="PC2", kind="Scatter", color="steelblue", ax=axes[0,0])
    # axes[0,0].set_title('PC1 vs PC2')
    df.plot(x="PC3", y="PC2", kind="Scatter", color="forestgreen", ax=axes[0,1])
    # axes[0,1].set_title('PC2 vs PC3')
    df.plot(x="PC1", y="PC3", kind="Scatter", color="orange", ax=axes[1,0])
    # axes[1,0].set_title('PC1 vs PC3')
    df.plot(x="PC3", y="PC4", kind="Scatter", color="purple", ax=axes[1,1])
    # axes[1,1].set_title('PC3 vs PC4')
    plt.savefig(plink_eigenvec + '.pca.pdf')
    plt.close(fig)
    # transform and safe first three eigen vectors for covariate estimate


def plot_pval(combiFile, df):
    '''
    plots
    '''
    print ("plotting pval distr")
    plt.figure()
    #plotNameQQ = os.path.basename(combiFile).replace('.txt', '_bpval_qqplot.pdf')
    plotNameHisto = os.path.basename(combiFile).replace('.txt', '__pval_hist.pdf')
    plotNameTitle = os.path.basename(combiFile).replace('.txt', '')
    #stats.probplot(df['bpval'], dist="norm", plot=plt)
    #plt.savefig(plotNameQQ)
    #plt.close()
    fig,ax = plt.subplots(2,2)
    ax[0,0].hist(df.bpval, bins=50, color="steelblue")
    ax[0,0].set_title('ppval')
    ax[0,0].set_xlim([0,1.2])
    ax[1,0].hist(df.fdr_bh, bins=50, color="purple")
    ax[1,0].set_title('FDR BH')
    ax[1,0].set_xlim([0,1.2])
    ax[0,1].hist(df.fdr_by, bins=50, color="orange")
    ax[0,1].set_title('FDR BY')
    ax[0,1].set_xlim([0,1.2])
    ax[1,1].hist(df.bonferroni, bins=50, color="firebrick")
    ax[1,1].set_title('FDR Bonferroni')
    ax[1,1].set_xlim([0,1.2])
    fig.suptitle(plotNameTitle, size=5)
    fig.savefig(plotNameHisto, figsize=(20,15))
    plt.close(fig)


def make_boxplot(df_fpkm, project, log_fpkm=True):
    gene_grouped = df_fpkm.groupby(by='gene_name')
    print ("generating boxplots of expression vs genotype")
    for gene, geneset in gene_grouped:
        try:
            ctrl = geneset.ix[geneset.sv == "ctrl", "fpkm"]
            cesam = geneset.ix[geneset.sv == "cesam", "fpkm"]
            r, pval = stats.ranksums(ctrl, cesam)
            ax = sns.boxplot(geneset.fpkm, groupby=geneset.sv,  palette={'cesam':'orange', 'ctrl':'steelblue'}, fliersize=0)
            ax = sns.stripplot(data=geneset, y="fpkm", x="sv",jitter=True, size=4, color=".3", linewidth=0)
            if log_fpkm:
                ax.set_yscale("log")
                ax.set_ylabel('fpkm (log)')
            plt.title(project + "\nGene=" + gene + "\nranksum pval=" + str(pval))
            fig_title = project + "_" + gene + "_boxplot_expression.jpg"
            outDir = 'genes/' + gene
            if not os.path.exists(outDir):
                os.makedirs(outDir)
            plt.savefig(outDir + "/" + fig_title, dpi=None, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format=None,
            transparent=False, bbox_inches=None, pad_inches=0.1,
            frameon=None)
            plt.close()
        except Exception, e:
            pass
    plt.close('all')

