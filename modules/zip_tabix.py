#!/bin/env python
from __future__ import print_function
import os
import subprocess
def zip_tabix(vcf, p="vcf"):
    '''
    bgzip and tabix vcf or bedfile using tabix -f -p
    '''
    vcfgz = vcf + '.gz'
    if os.path.exists(vcfgz):
        os.remove(vcfgz)
    if os.stat(vcf).st_size > 0:
        cmdZip = 'bgzip -f ' + vcf
        subprocess.Popen(cmdZip.split())
        try:
            os.wait()
        except Exception, e:
            pass
        if os.path.isfile(vcfgz):
            cmdtab = 'tabix -f -p ' + p + ' ' + vcfgz
            subprocess.Popen(cmdtab.split())
            os.wait()
            print ("bgzipped and tabix indexed", p, "\n", vcfgz)
    return vcfgz

