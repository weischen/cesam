#!/usr/bin/env python
## created: 2015-12
## by: Joachim Weischenfeldt
## joachim.weischenfeldt@gmail.com
from __future__ import print_function
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
#plt.style.use('ggplot')
import scipy.stats
qchisq = scipy.stats.chi2.ppf

def estimate_lambda(pval, method="median", df=1):
    '''
    estimate genomic inflation factor lambda from a array of pvalues
    input
    pval       :  pvalues (array)
    return
    gc_lambda  : genomic inflation factor lambda (float)
    '''
    if not isinstance(pval, np.ndarray):
        pval = np.array(pval)
    if method == 'zscore':
        chisq = pval**2
    elif method == 'chisq':
        chisq = pval
    else:
        chisq = qchisq(1 - pval, df)
    gc_lambda = np.median(chisq)/qchisq(0.5,1)
    return gc_lambda


def qqplot(pval, titlename="sample", filename="", xmax="", point2plot=""):
    '''
    draws a qqplot
    input:
    pval        : values (list) [0;1] to plot (required)
    titlename   : name (str) to be used in title and plot name (optional)
    filename    : name (str) of file to be saved (optional)
    xmax        : x and ylim (int) (optional)
    '''
    observ = np.sort(np.array(pval))
    lobserv = -(np.log10(observ))
    expect = np.arange(1, len(observ)+1)
    lexpect = -(np.log10(expect / float(len(expect)+1)))
    gc_lambda = estimate_lambda(observ, method="median", df=1)
    fig, ax = plt.subplots(figsize=(10,10), facecolor="white")
    plt.xlabel("Expected -log10(pval)")
    plt.ylabel("Observed -log10(pval)")
    plt.scatter(lexpect, lobserv, s=4, color="black")
    if point2plot:
        try:
            point2plotObs = -np.log10(point2plot)
            point2plotIndex = list(lobserv).index(point2plotObs)
            point2plotExp = lexpect[point2plotIndex]
            plt.scatter(point2plotExp, point2plotObs, s=10, color="red")
        except Exception, e:
            print ("could not plot the point2plot", point2plot)
    if xmax:
        plt.xlim([0,xmax])
        plt.ylim([0,xmax])
        plt.plot([0,xmax], [0,xmax], 'r-', linewidth=0.5)
    else:
        maxval = max(ax.get_xlim()[1], ax.get_ylim()[1])
        plt.xlim([0,ax.get_xlim()[1]])
        plt.ylim([0,ax.get_ylim()[1]])
        plt.plot([0,maxval], [0,maxval], 'r-', linewidth=0.5)
    plt.title(titlename + '\nqqplot\nlambda: ' + str(gc_lambda))
    if not filename:
        filename = titlename
    plotOut = filename + '_qqplot.pdf'
    plt.savefig(plotOut)
    plt.close()
    print ("qqplot saved as", plotOut)

